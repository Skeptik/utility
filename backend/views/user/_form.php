<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use common\models\Profile;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

if (!$model->status_id)
    $model->status_id = User::STATUS_ACTIVE;

$profile = $model->profile;

if (!$profile)
    $profile = new Profile();

if (!$profile->gender_id)
    $profile->gender_id = Profile::GENDER_M;
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'status_id')->radioList($model->statuses); ?>

    <?= $form->field($profile, 'gender_id')->radioList($profile->genders); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($profile, 'last_name')->textInput(); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($profile, 'first_name')->textInput(); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($profile, 'middle_name')->textInput(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'email'); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'phone')->textInput() ?>
        </div>
    </div>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('user', 'Add') : Yii::t('user', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
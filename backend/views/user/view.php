<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('user', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('user', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('user', 'Remove this user?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at:datetime',
            [
                'attribute' => 'status',
                'label' => Yii::t('user', 'Status ID'),
            ],
//            'profile.timezone',
//            'profile.timezone_offset',
            [
                'attribute' => 'fullName',
                'label' => Yii::t('user', 'Full Name'),
            ],
//            'profile.photo',
            [
                'attribute' => 'profile.gender',
                'label' => Yii::t('user', 'Gender ID'),
            ],
            [
                'attribute' => 'username',
                'label' => Yii::t('user', 'User Name'),
            ],
            'email:email',
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
        ],
    ]) ?>

</div>

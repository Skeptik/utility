<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('user', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('user', 'Add User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'disabledPageCssClass' => 'disabled',
            'pageCssClass' => 'page-link',
            'nextPageCssClass' => 'page-link',
            'prevPageCssClass' => 'page-link',
            'firstPageCssClass' => 'page-link',
            'lastPageCssClass' => 'page-link',
        ],
        'columns' => [
            'id',
            [
                'attribute' => 'created_at',
                'value' => 'created_at',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'type' => DatePicker::TYPE_INPUT,
                    'attribute'=>'created_at',
                    'language' => 'ru',
                    'options' => ['class' => 'form-control'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'status',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status_id',
                    [null => Yii::t('user', 'Not chosen')] + $searchModel->getStatuses(),
                    ['class' => 'form-control']),
            ],
            [
                'attribute' => 'fullName',
                'label' => Yii::t('user', 'Full Name'),
            ],
            'email:email',
            [
                'label' => 'Role',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a(
                        (Yii::$app->authManager->checkAccess($model->id, 'admin')?'Revoke':'Assing') . ' admin',
                        ['user/toggle-admin', 'id' => $model->id]
                    );
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
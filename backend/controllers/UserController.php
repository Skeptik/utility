<?php
namespace backend\controllers;

use common\models\Profile;
use Yii;
use common\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\UserSearch;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'roles' => ['admin'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->setPassword($model->password);
            $model->generateAuthKey();
            $model->username = $model->email;

            if ($model->save()) {

                $profile = $model->profile;
                if (!$profile) $profile = new Profile();
                $profile->load(Yii::$app->request->post());
                $profile->user_id = $model->id;
                $profile->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->password) $model->setPassword($model->password);
            $model->username = $model->email;

            if ($model->save()) {

                $profile = $model->profile;
                if (!$profile) $profile = new Profile();
                $profile->load(Yii::$app->request->post());
                $profile->user_id = $model->id;
                $profile->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($model = $this->findModel($id)) {
            $model->status_id = User::STATUS_DELETED;
            $model->save();
        }

        return $this->redirect(['index']);
    }

    public function actionToggleAdmin($id = 0)
    {
        if ($id && $user = User::find()->andWhere(['id' => $id])->one()) {
            $auth = Yii::$app->getAuthManager();
            $admin = $auth->getRole('admin');

            $adminUsers = $auth->getUserIdsByRole($admin->name);

            if (!in_array($user->id, $adminUsers) && $user->email !== 'info@wfstudio.ru')
            {
                $auth->assign($admin, $user->id);
                Yii::$app->session->setFlash('success', 'Админ назначен');
            }
            elseif ($user->email !== 'info@wfstudio.ru')
            {
                $auth->revoke($admin, $user->id);
                Yii::$app->session->setFlash('danger', 'Админ отзван');
            }
        }

        return $this->redirect('index');
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php
namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

class UserSearch extends User
{

    public $fullName = '';

    public function rules()
    {
        $rules = [];
        return array_merge($rules, [
            [['created_at'], 'safe'],
            [['fullName', 'email'], 'string'],
            [['status_id'], 'integer'],
        ]);
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = User::find()->alias('u');

        $this->load($params);

        if ($this->created_at && strtotime($this->created_at) > 0) {
            $query = $query->andWhere(['between', 'u.created_at'
                , date('Ymd000000', strtotime($this->created_at))
                , date('Ymd235959', strtotime($this->created_at))
            ]);
        }

        if($this->fullName) {
            $query->joinWith(['profile as p']);
            $query->andFilterWhere(['or'
                , ['like', 'p.last_name', htmlspecialchars($this->fullName)]
                , ['like', 'p.first_name', htmlspecialchars($this->fullName)]
                , ['like', 'p.middle_name', htmlspecialchars($this->fullName)]
            ]);
        }

        if($this->email)
            $query->andFilterWhere(['like', 'u.email', htmlspecialchars($this->email)]);

        if($this->status_id || $this->status_id == 0)
            $query->andFilterWhere(['u.status_id' => $this->status_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>[
                'defaultOrder'=>[
                    'created_at'=>SORT_DESC
                ]
            ]
        ]);

        return $dataProvider;
    }
}
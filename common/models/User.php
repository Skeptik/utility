<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{

    public $password = null;

    const ROLE_ADMIN = 'admin';
    const ROLE_REGISTERED = 'user';
    const ROLE_GUEST = 'guest';

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    private $_isAdmin = null;

    private $_statuses = [
        self::STATUS_ACTIVE => 'Активна',
        self::STATUS_DELETED => 'Удалена',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at','password'], 'safe'],
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'unique'],
            [['phone'], 'number'],
            ['status_id', 'default', 'value' => self::STATUS_ACTIVE],
            ['status_id', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['username','password'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'created_at' => Yii::t('user', 'Created At'),
            'password_hash' => Yii::t('user', 'Password Hash'),
            'email' => Yii::t('user', 'Email'),
            'phone' => Yii::t('user', 'Phone'),
            'status_id' => Yii::t('user', 'Status ID'),
            'password' => Yii::t('user', 'Password'),
            'status' => Yii::t('user', 'Status'),
        ];
    }

    static public function roleArray()
    {
        return [
            self::ROLE_ADMIN,
            self::ROLE_REGISTERED,
            self::ROLE_GUEST,
        ];
    }

    public function getRoles()
    {
        return \Yii::$app->authManager->getRolesByUser($this->id);
    }

    public function getRole()
    {
        $roles = @unserialize($this->roles);

        if (is_array($roles) && !empty($roles[0])) {
            if ($roles[0] == 'ROLE_ADMIN')
                return self::ROLE_ADMIN;
        }

        return self::ROLE_REGISTERED;
    }

    static public function getRoleOfUser($id)
    {
        if ($id == 1) return User::ROLE_ADMIN;
        else return User::ROLE_REGISTERED;


        $roleArray = (new Query())
            ->select('roles')
            ->from(self::tableName())
            ->where(['id' => $id])
            ->scalar();

        $roles = @unserialize($roleArray);

        if (is_array($roles) && !empty($roles[0])) {
            if ($roles[0] == 'ROLE_ADMIN')
                return User::ROLE_ADMIN;
        }

        return User::ROLE_REGISTERED;
    }

    public function getIsAdmin()
    {
        if ($this->_isAdmin === null)
            $this->_isAdmin = array_key_exists(self::ROLE_ADMIN, $this->getRoles());

        return $this->_isAdmin;
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status_id' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()->alias('u')
            ->joinWith('userTokens as tk')
            ->andWhere(['tk.bearer' => $token])
            ->andWhere('u.id = tk.user_id')
            ->one(Yii::$app->db);
    }

    /**
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status_id' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status_id' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
        return $this->password_reset_token;
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getName()
    {
        $fullname = trim($this->username);
        return $fullname;
    }

    public function getPhoneString()
    {
        if (empty($this->phone))
            return '';

        $number = preg_replace('/[^\d]+/siu', '', $this->phone);
        if (mb_strlen($number) < 10 || mb_strlen($number) > 11)
            return '';

        if (mb_strlen($number) === 10) {
            $number = '7' . $number;
        }

        $number = preg_replace('/^8/siu', '7', $number);

        return '+' . $number;
    }

    public function getStatuses()
    {
        return $this->_statuses;
    }

    public function getStatus($id=0)
    {
        if (!$id) $id = $this->status_id;
        return ((!empty($this->_statuses[$id])) ? $this->_statuses[$id] : Yii::t('user', 'No Specify'));
    }

    public function getUserTokens()
    {
        return $this->hasMany(UserToken::class, ['user_id' => 'id']);
    }

    public function addUserToken($fbase = '', $fbasePlatform = 'android')
    {
        $ut = new UserToken();
        $ut->user_id = $this->id;
        $ut->bearer = $this->generatePasswordResetToken();
        $ut->push_token = $fbase;
        $ut->platform = $fbasePlatform;

        $ut->save();

        return $ut->bearer;
    }

    public function getFullName()
    {
        $text = '';
        if (defined('BACKEND') && BACKEND)
            $text = '[ ' . $this->id . ' ] ';

        if ($this->profile)
            $text .= $this->profile->fullName;

        return $text;
    }
}

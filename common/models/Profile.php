<?php
namespace common\models;

use Yii;
use yii\db\Expression;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%profile}}".
 *
 * @property int $user_id
 * @property string $last_name
 * @property string $first_name
 * @property string $middle_name
 * @property string $photo
 * @property string $timezone
 * @property int $gender_id
 * @property int $timezone_offset
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    const GENDER_M = 1;
    const GENDER_G = 2;

    private $_genders = [
        self::GENDER_M => 'М',
        self::GENDER_G => 'Ж',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'last_name', 'first_name', 'middle_name', 'timezone', 'timezone_offset', 'created_at'], 'safe'],
            [['user_id', 'timezone_offset', 'status_id', 'gender_id'], 'integer'],
            [['last_name', 'first_name', 'middle_name', 'photo'], 'string', 'max' => 255],
            [['timezone'], 'string', 'max' => 24],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('user', 'User ID'),
            'last_name' => Yii::t('user', 'Last Name'),
            'first_name' => Yii::t('user', 'First Name'),
            'middle_name' => Yii::t('user', 'Middle Name'),
            'photo' => Yii::t('user', 'Photo'),
            'gender_id' => Yii::t('user', 'Gender ID'),
            'timezone' => Yii::t('user', 'Timezone'),
            'timezone_offset' => Yii::t('user', 'Timezone Offset'),
            'status_id' => Yii::t('user', 'Status ID'),
            'created_at' => Yii::t('user', 'Created At'),
            'fullName' => Yii::t('user', 'Full Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getUrl()
    {
        return ['/site/user', 'id' => $this->user_id];
    }

    public function getFullName($withLinkProfile = false)
    {
        $fullname = trim(preg_replace('/\s+/siu', ' ', $this->last_name . ' ' . $this->first_name . ' ' . $this->middle_name));

        return ($withLinkProfile) ? Html::a($fullname, $this->getUrl(), ['target' => '_blank']) : $fullname;
    }

    public function getFullNameCard($withLinkProfile = false)
    {
        return '[' . $this->user_id . '] ' . $this->getFullName($withLinkProfile);
    }

    public static function primaryKey()
    {
        return ['user_id'];
    }

    public function getGenders()
    {
        return $this->_genders;
    }

    public function getGender($id=0)
    {
        if (!$id) $id = Profile::GENDER_M;
        return ((!empty($this->_genders[$id])) ? $this->_genders[$id] : 'Не указано');
    }
}
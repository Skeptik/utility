<?php

use yii\db\Migration;

/**
 * Class m190404_140718_profile
 */
class m190404_140718_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%profile}}', [
            'user_id' => $this->integer(),
            'created_at' => $this->timestamp()->notNull(),
            'status_id' => $this->smallInteger(1)->notNull()->defaultValue(10),

            'last_name' => $this->string(255),
            'first_name' => $this->string(255),
            'middle_name' => $this->string(255),

            'photo' => $this->string(255),

            'gender_id' => $this->smallInteger(1),
            'timezone' => $this->string(24),
            'timezone_offset' => $this->integer(255),

        ], $tableOptions);

        $this->addForeignKey(
            'FK_user_profile',
            '{{%profile}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_user_profile', '{{%profile}}');
        $this->dropTable('{{%profile}}');
    }
}
<?php

use yii\db\Migration;

/**
 * Class m190404_134858_user_correct_fields
 */
class m190404_134858_user_correct_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%user}}', 'updated_at', $this->timestamp());
        $this->alterColumn('{{%user}}', 'status', $this->smallInteger(1)->notNull()->defaultValue(10));
        $this->renameColumn('{{%user}}', 'status', 'status_id');
        $this->alterColumn('{{%user}}', 'created_at', $this->timestamp()->notNull());
        $this->addColumn('{{%user}}', 'phone', $this->string(15));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'phone');
        $this->alterColumn('{{%user}}', 'created_at', $this->integer()->notNull());
        $this->renameColumn('{{%user}}', 'status_id', 'status');
        $this->alterColumn('{{%user}}', 'status', $this->smallInteger()->notNull()->defaultValue(10));
        $this->alterColumn('{{%user}}', 'updated_at', $this->integer()->notNull());
    }
}

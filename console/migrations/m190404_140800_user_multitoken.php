<?php

use yii\db\Migration;

/**
 * Class m190303_214313_user_multitoken
 */
class m190404_140800_user_multitoken extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_token}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp(),
            'user_id' => $this->integer(11)->notNull(),
            'bearer' => $this->string(128),
            'platform' => $this->string(16),
            'push_token' => $this->string(255),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_user_token',
            '{{%user_token}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_user_token', '{{%user_token}}');
        $this->dropTable('{{%user_token}}');
    }
}

<?php
namespace console\controllers;

use Yii;
use common\models\User;

class RbacController extends \yii\console\Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->getAuthManager();
        $auth->removeAll();

        $admin = $auth->createRole(User::ROLE_ADMIN);

        $auth->add($admin);

        $adminUser = User::find()->andWhere(['email' => 'Zhnecc@gmail.com'])->one();

        $auth->assign($admin, $adminUser->id);
    }
}
<?php
namespace console\controllers;

use common\models\User;
use common\models\Profile;

class ToolController extends \yii\console\Controller
{
    public function actionInit()
    {
        $user = new User();
        $user->username = 'skeptik';
        $user->status_id = User::STATUS_ACTIVE;
        $user->email = 'Zhnecc@gmail.com';
        $user->phone = '79771641323';
        $user->auth_key = '';
        $user->updated_at = '';
        $user->setPassword('skep');
        $user->save();

        $profile = new Profile();
        $profile->load(['Profile' => [
            'user_id' => $user->id,
            'last_name' => 'Skeptik',
            'first_name' => '',
            'middle_name' => '',
            'gender' => 1,
            'timezone' => 'Europe/Moscow',
            'timezone_offset' => 0,
        ]]);

        $profile->save();
    }
}
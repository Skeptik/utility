<?php
use yii\widgets\Menu;

$menu = [
    ['label' => Yii::t('site', 'Home'), 'url' => '#'],
    ['label' => Yii::t('site', 'About'), 'url' => '#'],
    ['label' => Yii::t('site', 'Services'), 'url' => '#'],
    ['label' => 'Blog <i class="ti-angle-down"></i>',
        'url' => 'javascript:void(0);',
        'items' => [
            ['label' => 'Blog', 'url' => '#'],
            ['label' => 'Single-blog', 'url' => '#'],
        ]
    ],
    ['label' => 'Pages <i class="ti-angle-down"></i>',
        'url' => 'javascript:void(0);',
        'items' => [
            ['label' => 'Elements', 'url' => '#'],
            ['label' => 'Portfolio', 'url' => '#'],
            ['label' => 'Portfolio details', 'url' => '#'],
        ]
    ],
    ['label' => Yii::t('site', 'Contact'), 'url' => '#'],
]; ?>

<nav>
    <?= Menu::widget([
        'items' => $menu,
        'encodeLabels' => false,
        'options' => ['id' => 'navigation'],
        'linkTemplate' => '<a href="{url}">{label}</a>',
        'submenuTemplate' => "<ul class='submenu' role='menu'>\n{items}\n</ul>\n",
    ]); ?>
</nav>
<?php
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets/public';

    public $css = [
        'css/owl.carousel.min.css'
        , 'css/magnific-popup.css'
        , 'css/font-awesome.min.css'
        , 'css/themify-icons.css'
        , 'css/nice-select.css'
        , 'css/flaticon.css'
        , 'css/gijgo.css'
        , 'css/animate.css'
        , 'css/slick.css'
        , 'css/slicknav.css'
        , 'css/style.css'
    ];

    public $js = [
        'js/vendor/modernizr-3.5.0.min.js'
        , 'js/popper.min.js'
        , 'js/owl.carousel.min.js'
        , 'js/isotope.pkgd.min.js'
        , 'js/ajax-form.js'
        , 'js/waypoints.min.js'
        , 'js/jquery.counterup.min.js'
        , 'js/imagesloaded.pkgd.min.js'
        , 'js/scrollIt.js'
        , 'js/jquery.scrollUp.min.js'
        , 'js/wow.min.js'
        , 'js/nice-select.min.js'
        , 'js/jquery.slicknav.min.js'
        , 'js/jquery.magnific-popup.min.js'
        , 'js/plugins.js'
        , 'js/gijgo.min.js'
        , 'js/slick.min.js'
        , 'js/contact.js'
        , 'js/jquery.ajaxchimp.min.js'
        , 'js/jquery.form.js'
        , 'js/jquery.validate.min.js'
        , 'js/mail-script.js'
        , 'js/main.js'
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
    ];
}
